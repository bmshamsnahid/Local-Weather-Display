$(document).ready( function() {
    function testMethod() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
        else {
            console.log('Geolocation is not supported for this Browser/OS.');
        }
    }

    function showPosition(position) {
        console.log("Latitude: " + position.coords.latitude + 
        "\nLongitude: " + position.coords.longitude); 

        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        var url = 'https://fcc-weather-api.glitch.me/api/current?lat='+ latitude +'&lon=' + longitude;
    
        $.ajax({
            dataType: 'jsonp',
            jsonpCallback: "parseQuote",
            url: url,
            success: function(response) {
              console.log(response);
              var locationName = response.name;
              var temperature = response.main.temp;
              var weatherInfo = response.weather[0].main;
              var weatherDescription = response.weather[0].description;
              var windSpeed = response.wind.speed;
              var weatherIcon = response.weather[0].icon;

              // weatherIcon = 'https://cdn.glitch.com/6e8889e5-7a72-48f0-a061-863548450de5%2F01n.png?1499366020783';

              $('#location-name').text(locationName);
              $('#temperature-in-celcious').text(temperature + 'C');
              $('#temperature-in-farenheight').text(temperature * (9/5) + 32 + 'F');
              $('.weather-icon').attr('src', weatherIcon);
              $('#weather-info').text(weatherDescription);
              $('#wind-speed').text('Wind Speed: ' + windSpeed);
              

              console.log('Location: ' + locationName);
              console.log('Temperature: ' + temperature);
              console.log('Weather Info: ' + weatherInfo);
              console.log('Weather Description: ' + weatherDescription);
              console.log('Wind Speed: ' + windSpeed);
              console.log('weather icon: ' + weatherIcon);
            }
          });   
    }

    testMethod();

});